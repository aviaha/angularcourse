import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class TodosService {
  delete(key){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos').remove(key);
  })}
  
  addTodo(text){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/todos').push({'text':text});
    })
  }
  
  constructor(private authService:AuthService,
              private db:AngularFireDatabase  ) { }
}
