import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from "@angular/router";


@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  email:string;
  password:string;
  name:string;
  error='';

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

  toregister(){
    //console.log("signup"+this.email+''+this.password+this.name+'')
    this.authService.toregister(this.email, this.password)
    .then(value =>{
      this.authService.updateProfile(value.user, this.name);
      this.authService.addUser(value.user, this.name);
    }).then(value=>{
      this.router.navigate(['/']);
      //ללכוד שגיאה
    }).catch(err=>{
        this.error =err;
        console.log(err);
        
    })
  }

}
