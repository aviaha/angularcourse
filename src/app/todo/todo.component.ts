import { Component, OnInit , Input, Output,EventEmitter} from '@angular/core';
import { TodosService } from '../todos.service';


@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  text;
  key;

  showTheButton = false;

  send(){
    this.myButtonClicked.emit(this.text);
  }
  showButton(){
    this.showTheButton =true;
  }
  hideButton(){
    this.showTheButton =false;
  }
  delete(){
  this.todosService.delete(this.key)
  }
  constructor(private todosService:TodosService) { }

  ngOnInit() {
    this.text=this.data.text;
    this.key=this.data.$key;
  }
  
}
